<?php

// Include composer dependencies
include_once __DIR__ . '/vendor/autoload.php';
include 'functions.php';

// Init router
$router = new AltoRouter();

// Router mapping
$router->map('GET', '/', function() {}, 'home');
$router->map('GET', '/[i:id]', function() {}, 'disclaimer');
$router->map('GET', '/b[i:id]', function() {}, 'disclaimer_beta');

// Router matching
$match = $router->match();

// Template engine settings and variables
$options = [
    'paths' => [
        'views/',
    ],
    'enable_profiler' => false,
    'profiler' => [
        'time_precision' => 3,
        'line_height'    => 30,
        'display'        => true,
        'log'            => false,
    ],  
];
$variables = [];

// If we are dealing with home
if ($match['name'] == 'home' || $match['name'] == 'disclaimer') {
    $variables['title'] = 'SMDL.io';
    $variables['disclaimers'] = getDisclaimers($variables['params']);
}

// If we are dealing with a dislcaimer link
if ($match['name'] == 'disclaimer_beta') {
    $variables['title'] = '#'.$match['params']['id'].' - SMDL.io';
    $variables['params']['id'] = $match['params']['id'];
    $variables['disclaimers'] = getDisclaimers($variables['params']);
}

// Render the appropriate route
if(is_array($match) && is_callable($match['target'])) {
    switch ($match['name']) {
        case 'home':
        case 'disclaimer':
            Phug::displayFile('index', $variables, $options);
            break;
    
        case 'disclaimer_beta':
            Phug::displayFile('disclaimer', $variables, $options);
            break;
    }
} else {
    // No route was matched
    Phug::displayFile('404', $variables, $options);
}