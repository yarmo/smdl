<?php

// Include composer dependencies
include_once __DIR__ . '/vendor/autoload.php';
use Pagerange\Markdown\MetaParsedown;

// Functions
function getDisclaimers($params) {
    
    /**
     * 
     */

    $mp = new MetaParsedown();

    if (isset($params['id'])) {
        $id = sprintf('%04d', $params['id']);
        $content = file_get_contents('disclaimers_beta/' . $id . '.md');
        $meta = $mp->meta($content);
        
        $item = array(
            "id" => $meta["id"],
            "listed" => $meta["listed"],
            "url" => 'https://'.$_SERVER['SERVER_NAME'].'/b'.$meta["id"],
            "content" => $mp->text($content));
        
        return $item;
    }
    
    $posts = array();
    
    $files = scandir('disclaimers_beta/');
    foreach($files as $file) {
        // Skip . and ..
        if (($file == '.') || ($file == '..')) {
            continue;
        }
        
        $content = file_get_contents('disclaimers_beta/' . $file);
        $meta = $mp->meta($content);

        $item = array(
                    "id" => $meta["id"],
                    "listed" => $meta["listed"],
                    "url" => 'https://'.$_SERVER['SERVER_NAME'].'/b'.$meta["id"],
                    "content" => $mp->text($content));

        if (!$item['listed']) {
            continue;
        }

        $items[] = $item;
    }

    return $items;
}